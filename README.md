Divine Hub 2016 © The Divine Minecraft Services LLC
--------
Divine Hub is the main plugin for The Divine's hub, and is the one that provides a ton of the gadgets, cancels dropping blocks, etc.
The plugin is pretty light-weight and is maintained by Akelov.
Nothing much else to say here besides that this project exists so the public can view our work.
Compiling this project/reverse engineering is looked down on.
View our license file for licensing information.